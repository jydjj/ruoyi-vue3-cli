const { defineConfig } = require("@vue/cli-service");
const AutoImport = require("unplugin-auto-import/webpack");
const Components = require("unplugin-vue-components/webpack");
const CompressionPlugin = require("compression-webpack-plugin");
const path = require("path");

module.exports = defineConfig({
  transpileDependencies: true,
  configureWebpack: (config) => {
    config.name = process.env.VUE_APP_TITLE;
    config.plugins.push(
      ...[
        AutoImport({
          imports: ["vue", "vue-router", "pinia"],
          dts: true,
        }),
        Components({
          dts: true,
        }),
        // http://doc.ruoyi.vip/ruoyi-vue/other/faq.html#使用gzip解压缩静态文件
        new CompressionPlugin({
          test: /\.(js|css|html)?$/i, // 压缩文件格式
          filename: "[path][base].gz", // 压缩后的文件名
          algorithm: "gzip", // 使用gzip压缩
          minRatio: 0.8, // 压缩率小于1才会压缩
          threshold: 10240, // 小于10KB就不进行压缩
          // deleteOriginalAssets: true, // 删除源文件
        }),
      ]
    );
  },
  chainWebpack: (config) => {
    // set svg-sprite-loader
    config.module
      .rule("svg")
      .exclude.add(path.resolve(process.cwd(), "src/assets/icons"))
      .end();
    config.module
      .rule("icons")
      .test(/\.svg$/)
      .include.add(path.resolve(process.cwd(), "src/assets/icons"))
      .end()
      .use("svg-sprite-loader")
      .loader("svg-sprite-loader")
      .options({
        symbolId: "icon-[name]",
      })
      .end();
  },
  devServer: {
    host: "0.0.0.0",
    port: 80,
    open: false,
    proxy: {
      // detail: https://cli.vuejs.org/config/#devserver-proxy
      [process.env.VUE_APP_BASE_API]: {
        target: process.env.VUE_APP_PROXY_TARGET,
        changeOrigin: true,
        pathRewrite: { ["^" + process.env.VUE_APP_BASE_API]: "" },
      },
    },
  },
});
