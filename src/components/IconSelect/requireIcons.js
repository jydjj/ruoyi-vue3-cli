const req = require.context("@/assets/icons/svg", false, /\.svg$/);
const svg = /\.\/(.*)\.svg/;

const requireAll = (ctx) => ctx.keys().map((i) => i.match(svg)[1]);

const icons = requireAll(req);

export default icons;
