// register globally
const req = require.context("@/assets/icons/svg", false, /\.svg$/);

const requireAll = (ctx) => ctx.keys().map(ctx);

requireAll(req);
